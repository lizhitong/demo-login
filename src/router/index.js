import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/Login.vue'

import store from "../store"
Vue.use(VueRouter)

const routes = [
  {
    path: "/",
    name: "Index",
    component: () => import("../views/Home.vue"),
    children: [
      {
        path: "users",
        name: "Users",
        component: () => import("../views/Users.vue"),
      },
      {
        path: "roles",
        name: "Roles",
        component: () => import("../views/rights/Roles.vue"),
      },
      {
        path: "rights",
        name: "Rights",
        component: () => import("../views/rights/Index.vue"),
      },
      {
        path: "goods",
        name: "Goods",
        component: () => import("../views/goods/Index.vue"),
      },
      {
        path: "goods/add",
        name: "Add",
        component: () => import("../views/goods/Add.vue"),
      },
      {
        path: "params",
        name: "Params",
        component: () => import("../views/goods/Params.vue"),
      },
      {
        path: "categories",
        name: "Categories",
        component: () => import("../views/goods/Categories.vue"),
      },
      {
        path: "orders",
        name: "Orders",
        component: () => import("../views/orders/Index.vue"),
      },
      {
        path: "reports",
        name: "Reports",
        component: () => import("../views/reports/Index.vue"),
      },
    ]
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },

]

const router = new VueRouter({
  routes
})

router.beforeEach((to, from, next) => {
  if (to.name === "Login") {
    next()
  } else {
    if (store.state.token) {
      next()
    } else {
      next({
        name: "Login"
      })
    }
  }
})


export default router
