import axios from 'axios'
import { Message } from 'element-ui'
import store from '../store'
import router from '../router'
const instance = axios.create({
  baseURL: process.env.VUE_APP_baseURL
})
const http = (url, data, method = 'GET', params) => {
  return instance({
    url,
    method,
    data,
    params,
    headers: {
      Authorization: store.state.token
    }
  }).then(res => {
    if (res.status >= 200 && res.status < 300 || res.status === 304) {
      if (res.data.meta.status === 200 || res.data.meta.status === 201) {
        return res.data
      } else {
        Message({
          message: res.data.meta.msg,
          type: 'error'
        })
        Promise.reject(res.data.meta.msg)
      }
    } else {
      Message({
        message: res.statusText,
        type: 'error'
      })
    }
  })
}
export default http