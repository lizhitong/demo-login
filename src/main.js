import Vue from 'vue'
import App from './App.vue'
import router from './router'
import "reset-css"
import ElementUI from "element-ui"
import "element-ui/lib/theme-chalk/index.css"
// import axios from "axios"
import instance from "./http"

import TreeTable from 'vue-table-with-tree-grid'
Vue.component('tree-table', TreeTable)

// 导入富文本编辑器
import VueQuillEditor from 'vue-quill-editor'
// 导入富文本编辑器对应的样式
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'
// 将富文本编辑器注册为全局可用的组件
Vue.use(VueQuillEditor)


// store in not  default 需要写下边这个
import store from './store'
// 面包屑
import BreadCrumb from "./components/BreadCrumb.vue"
Vue.component('bread-crumb',BreadCrumb)

//  假设开发接口      全局axios或者自定义实例默认值都可以
      // 全局的axios默认值
//     axios.defaults.baseURL="http://localhost:8889/api/private/v1/"
// //  假设测试 接口
//    axios.defaults.baseURL="http://localhost:8889/api/private/v1/"
// //  假设线上接口
//    axios.defaults.baseURL="http://localhost:8889/api/private/v1/"
// Vue.prototype.$axios=axios

// 自定义实例默认值
// var baseURL;      相当于把它们这个 process.env.NODE_ENV==="development"变成env文件,
// if(process.env.NODE_ENV==="development"){ 
//   baseURL="http://localhost:8889/api/private/v1/"
// }else if(process.env.NODE_ENV==="production"){
//   baseURL="http://localhost:8888/api/private/v1/"
// }else {
//   baseURL="http://localhost:8887/api/private/v1/"
// }


Vue.prototype.$axios=instance

 
Vue.use(ElementUI)
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
